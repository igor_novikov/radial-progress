import React, { Component } from 'react';

import { clamp } from '../util'
import RadialProgress from './RadialProgress'

export class Hello extends Component {
	constructor() {
		super()
		this.state = { progress: 63 }
	}

	changeProgress (progress) {
		progress = clamp (progress, 0, 100)
		this.setState ({ progress })
	}

	render() {
		return <div className="container">
			<div>
				<h1>Radial Progress Bar</h1>
				<h2>Or is it a radial rating bar?</h2>
			</div>
			<RadialProgress
				value={ this.state.progress }
				decimalPoint="1"
			/>
			<input
				type="number"
				value={ this.state.progress }
				onInput={ e => this.changeProgress (e.target.value) }
			/>
		</div>
	}
}
