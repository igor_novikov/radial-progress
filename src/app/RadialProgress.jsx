import React from 'react'

const insertDecimalPoint = (value, decimalPoint) => {
	console.log ('dpoint', value, decimalPoint)
	if (decimalPoint) {
		for (let i = 0; i < decimalPoint; i ++) value /= 10
	}
	return value.toFixed (decimalPoint)
}

const RadialProgress = ({ value, decimalPoint }) => (
	<div className={ 'radial-progress radial-progress-value-' + Math.round (value) }>
		<div className="progress-bar">
			<div></div>
		</div>
		<div className="progress-bar">
			<div></div>
		</div>

		<span>{ insertDecimalPoint (value, decimalPoint) }</span>
	</div>
)

export default RadialProgress
