import React from 'react';
import ReactDOM from 'react-dom';

import {Hello} from './app/Hello';

import './index.sass';

ReactDOM.render(
	<Hello/>,
	document.getElementById('root')
);
