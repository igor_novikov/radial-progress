const webpack = require ('webpack');
const conf = require ('./gulp.conf');
const path = require ('path');

const HtmlWebpackPlugin = require ('html-webpack-plugin');
const FailPlugin = require ('webpack-fail-plugin');
const autoprefixer = require ('autoprefixer');

module.exports = {
	module: {
		loaders: [
			{
				test: /\.json$/,
				loaders: [
					'json-loader'
				]
			},
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				loader: 'eslint-loader',
				enforce: 'pre'
			},
			{
				test: /\.(css|scss|sass)$/,
				loaders: [
					'style-loader',
					'css-loader',
					'sass-loader'
				]
			},
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				loaders: [
					'react-hot-loader',
					'babel-loader'
				]
			}
		]
	},
	resolve: {
		extensions: ['.js', '.jsx'],
	},
	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		FailPlugin,
		new HtmlWebpackPlugin({
			template: conf.path.src('index.html')
		}),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.LoaderOptionsPlugin({
			options: {
				postcss: () => [autoprefixer]
			},
			debug: true
		})
	],
	devtool: 'source-map',
	output: {
		path: path.join(process.cwd(), conf.paths.tmp),
		filename: 'index.jsx'
	},
	entry: [
		'webpack/hot/dev-server',
		'webpack-hot-middleware/client',
		`./${conf.path.src('index')}`
	]
};
